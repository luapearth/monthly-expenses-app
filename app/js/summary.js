var ref = new Firebase('https://monthly-expenses-app.firebaseio.com');

var userData = ref.getAuth(),
	displayName = "",
	startYear = 0,
	endYear = 0,
	runSearch = true,
	chartObjData = [],
	entries = 0;

if (userData.provider === "password") {
	displayName = userData.password.email.toUpperCase();
} else if (userData.provider === "facebook") {
	displayName = userData.facebook.displayName.toUpperCase();
}

if (!userData) {
	window.location = "/";
}

function getFirstDay(cDate) {
	date = new Date(cDate), y = date.getFullYear(), m = date.getMonth();
	return moment(new Date(y, m, 1)).valueOf();
}

function getLastDay(cDate) {
	date = new Date(cDate), y = date.getFullYear(), m = date.getMonth();
	return moment(new Date(y, m + 1, 0)).valueOf();
}

function serializeData() {
	if ( chartObjData.length < 12 ) {
		var cStartDate = new Date(startYear, chartObjData.length, 1),
			dStart = getFirstDay(cStartDate),
			dEnd = getLastDay(cStartDate);

		ref.child('users/' + userData.uid)
			.child('Expenses')
			.orderByChild('dateStamp')
			.startAt(dStart)
			.endAt(dEnd)
			.once('value', function(response) {
				var data = response.val();

				if (data !== null) {
					var water = 0,
						electricity = 0,
						food = 0,
						miscellaneous = 0,
						pointer = 0;
					$.each(data, function(idx, val) {
						pointer++;
						switch (val.type) {
							case "Food":
								food += (val.amount * 1);
								break;
							case "Water":
								water += (val.amount * 1);
								break;
							case "Electricity":
								electricity += (val.amount * 1);
								break;
							case "Miscellaneous":
								miscellaneous += (val.amount * 1);
								break;
						}
						if ( Object.keys(data).length == pointer ) {
							var init = {
								"month": getStringMonth(chartObjData.length),
								"water": water,
								"electricity": electricity,
								"food": food,
								"miscellaneous": miscellaneous
							}
							chartObjData.push(init);
							serializeData();
						}
					});
				} else {
					var init = {
						"month": getStringMonth(chartObjData.length),
						"water": 0,
						"electricity": 0,
						"food": 0,
						"miscellaneous": 0
					}
					chartObjData.push(init);
					serializeData();
				}
			});
	}

	// render
	var chart = AmCharts.makeChart("chartdiv", {
        "type": "serial",
        "theme": "none",
        "legend": {
            "horizontalGap": 10,
            "borderAlpha": 0.2,
            // "maxColumns": 2,
            // "position": "right",
            // "userGraphsettings": true,
            "markerSize": 10
        },
        "dataProvider": chartObjData,
        "valueAxes": [{
            "stackType": "regular",
            "axisAlpha": 0.3,
            "gridAlpha": 0
        }],
        "graphs": [
            {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Electricity",
                "type": "column",
                "color": "#000000",
                "valueField": "electricity"
            },
            {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Water",
                "type": "column",
                "color": "#000000",
                "valueField": "water"
            },
            {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Food",
                "type": "column",
                "color": "#000000",
                "valueField": "food"
            },
            {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Miscellaneous",
                "type": "column",
                "color": "#000000",
                "valueField": "miscellaneous"
            }
        ],
        "categoryField": "month",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "position": "left"
        }
    });
}

function getStringMonth(i) {
	switch (i) {
		case 0:
			return "Jan.";
			break;
		case 1:
			return "Feb.";
			break;
		case 2:
			return "March";
			break;
		case 3:
			return "April";
			break;
		case 4:
			return "May";
			break;
		case 5:
			return "June";
			break;
		case 6:
			return "July";
			break;
		case 7:
			return "Aug.";
			break;
		case 8:
			return "Sept.";
			break;
		case 9:
			return "Oct.";
			break;
		case 10:
			return "Nov.";
			break;
		case 11:
			return "Dec.";
			break;
	}
}


$(function() {
	var cal = $("#calVal").val(new Date());

	startYear = new Date($("#calVal").val()).getFullYear();

	$('#userEmail').html(displayName);
	
	$('#logout').click(function(e) {
		e.preventDefault();

		ref.unauth();

		window.location = "/";
	});

	$('#calendarChart').datepicker({
		todayHighlight: true,
		minViewMode: "years"
	}).on('changeDate', function(e) {
		if ( new Date(e.format("mm/dd/yyyy")).getFullYear() !== startYear && chartObjData.length === 12 ) {
			chartObjData = [];
			$('#calVal').val(new Date(e.format("mm/dd/yyyy")));
			startYear = new Date($("#calVal").val()).getFullYear();
			serializeData();
		} else {
			alert('Chart still loading, please wait and click the desire month when its done!');
		}
	});

	serializeData();
});
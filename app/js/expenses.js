var ref = new Firebase('https://monthly-expenses-app.firebaseio.com');

var userData = ref.getAuth(),
	profile = null,
	user = null,
	displayName = "";

if (userData.provider === "password") {
	displayName = userData.password.email.toUpperCase();
} else if (userData.provider === "facebook") {
	displayName = userData.facebook.displayName.toUpperCase();
}

	
if (!userData) {
	window.location = "/";
}

profile = ref.child('users/' + userData.uid);

function getFirstDay(cDate) {
	date = new Date(cDate), y = date.getFullYear(), m = date.getMonth();
	return moment(new Date(y, m, 1)).valueOf();
}

function getLastDay(cDate) {
	date = new Date(cDate), y = date.getFullYear(), m = date.getMonth();
	return moment(new Date(y, m + 1, 0)).valueOf();
}

$(function() {
	var cal = $("#calVal");
	cal.val(new Date());
	ref.child('users/' + userData.uid)
		.child('Expenses')
		.on("value", function(snapshot) {
		if (snapshot.val() != null) {
			calendarChange();
		} else {
			$("#list").empty();
			var chart = AmCharts.makeChart("chartdiv", {
			    "type": "pie",
				"theme": "none",
			    "dataProvider": [{"type": "You don't have any expense yet", "amount":1}],
			    "valueField": "amount",
			    "titleField": "type",
				"exportConfig":{	
			      menuItems: [{
			      icon: '/lib/3/images/export.png',
			      format: 'png'	  
			      }]  
				}
			});
		}
	});

	$('#userEmail').html(displayName);

	$('#logout').click(function(e) {
		e.preventDefault();

		ref.unauth();

		window.location = "/";
	});

	$('#expenseDate').datepicker({
		autoclose: true
	});

	$('#saveNew').click(function(e) {
		e.preventDefault();
		// console.log(moment(new Date($('#expenseDate').val())).valueOf(), moment($('#expenseDate').val(), "MM/DD/YYYY").format('LL'), $('#expenseDate').val());
		var x = profile.child('Expenses');
		// console.log(moment(new Date($('#expenseDate').val())).valueOf());
		x.push({
			dateStamp: moment(new Date($('#expenseDate').val())).valueOf(),
			date: moment($('#expenseDate').val(), "MM/DD/YYYY").format('LL'),
			amount: $('#expenseAmount').val(),
			desc: $('#expenseDesc').val(),
			type: $('#expenseType').val()
		});

		$('#expenseDate').val("");
		$('#expenseAmount').val("");
		$('#expenseDesc').val("");
		$('#expenseType').val("");
		$('#newEx').modal('hide');
	});

	$('#calendarChart').datepicker({
		todayHighlight: true,
		minViewMode: "months"
	}).on('changeDate', function(e) {
		if ( new Date(e.format("mm/dd/yyyy")).getMonth() !== 
			new Date($('#calVal').val()).getMonth() ) {
			$('#calVal').val(new Date(e.format("mm/dd/yyyy")));
			calendarChange();
		}
	});

	function calendarChange() {
		ref.child('users/' + userData.uid)
			.child('Expenses')
			.orderByChild('dateStamp')
			.startAt(getFirstDay(new Date(new Date(cal.val()))))
			.endAt(getLastDay(new Date(new Date(cal.val()))))
			.once("value", function(snapshot) {
			if (snapshot.val() != null) {
				$("#list").empty();
				var s = getFirstDay(new Date(new Date(cal.val())));
				var l = getLastDay(new Date(new Date(cal.val())));
				chartDatas = [{"type": "Electricity", "amount": 0},{"type": "Water", "amount": 0},{"type": "Food", "amount": 0},{"type": "Miscellaneous", "amount": 0}];	
				
				$.each(snapshot.val(), function(i, v) {
					// if (v.dateStamp >= s && v.dateStamp <= l) {
						$.each(chartDatas, function(ci, cv) {
							if (cv.type == v.type) {
								chartDatas.splice(ci, 1, {type: v.type, amount: (v.amount * 1) + cv.amount});
							}
						})
					// }
					var $tr = $("<tr />");
					var $td1 = $("<td />");
					var $td2 = $("<td />");
					var $td3 = $("<td />");
					var $td4 = $("<td />");
					var $td5 = $("<td />");
					$td1.html(v.date);
					$td2.html(v.type);
					$td3.html(v.amount);
					$td4.html(v.desc);
					$td5.html("<button type=\"button\" class=\"btn btn-danger btn-xs remove\" data-item=\"" + i + "\"><i class=\"fa fa-trash\"></i></button>");

					$tr.append($td1).append($td2).append($td3).append($td4).append($td5);
					$("#list").append($tr);
				});

				if (snapshot.val() !== null) {
					$('.remove').on('click', function() {
						if (confirm("Are you sure?")) {;
							var k = $(this).attr("data-item");
							var item = profile.child("Expenses/" + k);
							item.remove();
						}
					});
				}

				var chart = AmCharts.makeChart("chartdiv", {
				    "type": "pie",
					"theme": "none",
				    "dataProvider": chartDatas,
				    "valueField": "amount",
				    "titleField": "type",
					"exportConfig":{	
				      menuItems: [{
				      icon: '/lib/3/images/export.png',
				      format: 'png'	  
				      }]  
					}
				});
			} else {
				$("#list").empty();
				var chart = AmCharts.makeChart("chartdiv", {
				    "type": "pie",
					"theme": "none",
				    "dataProvider": [{"type": "You don't have any expense yet", "amount":100}],
				    "valueField": "amount",
				    "titleField": "type",
					"exportConfig":{	
				      menuItems: [{
				      icon: '/lib/3/images/export.png',
				      format: 'png'	  
				      }]  
					}
				});
			}
		});
	}	
});
var ref = new Firebase('https://monthly-expenses-app.firebaseio.com');

var userData = ref.getAuth();

if (userData) {
	window.location = "expenses.html";
}

$(function() {
	$('#login').click(function(e) {
		e.preventDefault();

		var email = $('#email').val(), password = $('#password').val();
		if (email != "" && password != "") {
			$('#login i').removeClass('fa-sign-in').addClass('fa-spinner').addClass('fa-pulse');
 			$('#login').addClass('disabled');
 			$('#register').addClass('disabled');
 			$('#fblogin').addClass('disabled');
			ref.authWithPassword({
				email: email,
				password: password
			}, function(error, authData) {
				if (error) {
					$('#login i').removeClass('fa-spinner').removeClass('fa-pulse').addClass('fa-sign-in');
	 				$('#login').removeClass('disabled');
	 				$('#register').removeClass('disabled');
	 				$('#fblogin').removeClass('disabled');
					alert('ERROR: ' + error);
				} else {
					window.location = "expenses.html";
				}
			});
		}
	});

	$("#password").keyup(function(e) {
		if (e.keyCode === 13) {
			if ( $("#email").val() !== "" && $("#password").val() !== "" ) {
				$("#login").click();
			}
		}
	});

	$("#fblogin").click(function() {
		ref.authWithOAuthPopup("facebook", function(error, authData) { 
			if (error) {
				console.log("Login Failed!", error);
			} else {
				window.location = "expenses.html";
			}
		}, {
			remember: "sessionOnly",
			scope: "email,user_likes"
		});
	});

	// $("#gpluslogin").click(function() {
	// 	ref.authWithOAuthPopup("google", function(error, authData) { 
	// 		if (error) {
	// 			console.log("Login Failed!", error);
	// 		} else {
	// 			window.location = "expenses.html";
	// 		}
	// 	}, {
	// 		remember: "sessionOnly",
	// 		scope: "email"
	// 	});
	// });

	$('#register').click(function(e) {
		e.preventDefault();

		var email = $('#email').val(), password = $('#password').val();
		$('#register i').removeClass('fa-user-plus').addClass('fa-spinner').addClass('fa-pulse');
		$('#register').addClass('disabled');
		$('#login').addClass('disabled');
		$('#fblogin').addClass('disabled');
		ref.createUser({
		  email: email,
		  password: password
		}, function(error, userData) {
			if (error){
				$('#register i').removeClass('fa-spinner').removeClass('fa-pulse').addClass('fa-user-plus');
				$('#register').removeClass('disabled');
				$('#login').removeClass('disabled');
				$('#register').removeClass('disabled');
		  		alert("Error: ", error);
			} else {
				ref.authWithPassword({
					email: email,
					password: password
				}, function(error, authData) {
					if (error) {
						$('#login i').removeClass('fa-spinner').removeClass('fa-pulse').addClass('fa-sign-in');
		 				$('#login').removeClass('disabled');
		 				$('#register').removeClass('disabled');
						alert('ERROR: ' + error);
					} else {
						window.location = "expenses.html";
					}
				});
			}
		});
	});
});
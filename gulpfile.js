var gulp = require('gulp'),
	webserver =require('gulp-webserver');

gulp.task('webserver', function() {
	gulp.src('./app')
	.pipe(webserver({
		livereload: true,
		open: true,
		port: 12504,
		fallback: 'index.html'
	}));
});

gulp.task('default', ['webserver']);